def print_title():
    print()
    print("------------------------------   THE GODS CALCULATOR  --------------------------------")
    print("This is calculating the greatest odd divisors sum (gods) of numbers in given interval.")
    print()


def input_prompt_int(message):
    number = 0
    while True:
        try:
            number = int(input(message))
        except ValueError:
            print("Sorry, I didn\'t understand that. Did you enter an integer?")
            continue
        else:
            break
    return number


def input_greater_int(message, lower):
    upper = input_prompt_int(message)
    while upper <= lower:
        print("{0} is not greater than {1}!  Please try again!".format(upper, lower))
        upper = input_prompt_int(message)
    return upper


def square(num):
    return num * num


def greatest_odd_divisor(number):
    number = abs(number)
    if number == 0:
        return 0
    if number % 2 == 1:
        return number
    for i in range(number, 0, -1):
        if number % i == 0 and i % 2 == 1:
            return i


def get_divisors(numbers):
    divisors = []
    for i in numbers:
        divisors.append(greatest_odd_divisor(i))
    return divisors


def pretty_print(numbers, divisors, sum):
    format_row = "{:10}" + ("{:" + str(len(str(max(numbers))) + 2) + "}") * len(numbers)

    print()
    print("Interval:\t[{0}, {1}]".format(numbers[0], numbers[-1]))
    print(format_row.format("Numbers:", *numbers))
    print(format_row.format("Divisors:", *divisors))
    print(("{:10}{:" + str(len(str(sum)) + 3) + "}").format("Sum:", sum))
    print()
    print("Sum of all greatest odd divisors for numbers form {} to {} is {}.".format(lower_bound, upper_bound,
                                                                                     sum_of_divisors))


if __name__ == '__main__':
    print_title()

    lower_bound = input_prompt_int("Enter lower interval bound:\t")
    upper_bound = input_greater_int("Enter upper interval bound:\t", lower_bound)

    numbers = range(lower_bound, upper_bound + 1)
    divisors = get_divisors(numbers)
    sum_of_divisors = sum(divisors)

    pretty_print(numbers, divisors, sum_of_divisors)
